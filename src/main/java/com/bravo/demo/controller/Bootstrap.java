package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Bootstrap {

    @GetMapping("/testBoot")
    public String nav(){
        return "fragments/navbar";
    }
//    @GetMapping("/testBoot")
//    public String header() {
//        return "fragments/header";
//    }
//    @GetMapping("/testBoot")
//    public String footer() {
//        return "fragments/footer";
//    }
}
