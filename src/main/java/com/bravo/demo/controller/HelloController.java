package com.bravo.demo.controller;


import com.bravo.demo.repositiories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {


    @Autowired
    UserRepository adsRepo;

    @RequestMapping("/Hello")
    public String home(Model model){

        model.addAttribute("Hello", adsRepo.findAll());
        return "Hello";
    }

}
