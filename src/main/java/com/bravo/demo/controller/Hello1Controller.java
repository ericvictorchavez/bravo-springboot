package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller

public class Hello1Controller {
    @GetMapping(path = "/hello1/{user}")
    @ResponseBody
    public String hello(@PathVariable String user){
        return "Hello" + user;
    }
}
