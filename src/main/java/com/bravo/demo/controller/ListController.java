package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ListController {

    //We will go over how iterate over a list and display to the view

    @GetMapping("hello-names")
    public String helloNames(Model model){//to pass a variable create

        List<String>names=new ArrayList<>();
        names.add("Adrian");
        names.add("MaryAnn");
        names.add("Sandra");
        names.add("Jonathan");
        names.add("Eric");
        names.add("Henry");
        model.addAttribute("names", names);

        return "List";







    }
}
