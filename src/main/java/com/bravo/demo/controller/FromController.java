package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FromController {
    @GetMapping("/show-form")
    public String showTheForm(){//METHOD ATTACHED TO URL

        return "company-form";//HTML FILE NAME
    }

    // mapping to process post the information from the HTML form
    @PostMapping("/process-form")
    public String processForm(@RequestParam(name="firstName") String firstName,
                              @RequestParam(name="lastName") String lastName,
                              @RequestParam(name="city") String city,
                              @RequestParam(name="state") String state,
                              @RequestParam(name="email") String email,
                              Model infoModel){
                            infoModel.addAttribute("name", "Name: " + firstName + " " + lastName);
                            infoModel.addAttribute("hometown", "Hometown: " + city + " " + state);
                            infoModel.addAttribute("contact" , "Email: " + email);
                            //the first parameter in .addAttribute matches up with the HTML variable that is used in the following syntax in HTML example = th:text="${contact}"
                            //The second parameter is the convent that will be returned on the HTML example - "Name: "
                            return "show-submit-form";
    }




}
