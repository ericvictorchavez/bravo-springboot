package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
//EXAMPLE OF HOW TO GET DATA FROM THE VIEW
@Controller//CONTROLLER
public class FormController {//FILE CLASS

    @GetMapping("/join")//URL PATH
    public String showJoinForm(){//METHOD ATTACHED TO URL
        return "join";//HTML FILE NAME
    }

    @PostMapping("/join")//post mapping is used to post data to the view
    public String joinCohort(@RequestParam(name = "cohort")String cohort, Model model){
        //@RequestParam annotation is used to read the form data
        model.addAttribute("cohort","Welcome to " + cohort + "!");

        return "join";

    }




}
