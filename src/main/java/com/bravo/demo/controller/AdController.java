package com.bravo.demo.controller;

import com.bravo.demo.model.Ad;
import com.bravo.demo.repositiories.AdRepo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AdController {

    private AdRepo adDao;

    public AdController(AdRepo adDao){
        this.adDao = adDao;
    }
//    the above code is using dependency injection-passing objects into the constructor of an object

    @GetMapping("/ads")
    @ResponseBody
    public List<Ad>getAllAds(){
        return adDao.findAll();
    }

//    @GetMapping("/ads/search/{title}")
//    public String searchAd(@PathVariable String title, Model model) {
//        model.addAttribute("ad", adDao.findByTitle(title));
//        return "ads/search";
//    }



}
