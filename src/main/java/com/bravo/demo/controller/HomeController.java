package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

//    @GetMapping("/")//GetMapping annotation to define what url map this method is identifies with.
//
//    @ResponseBody//defines what the response will be once the url is given
//    public String land(){//the method called on the response body annotation
//
//        return "Welcome to my landing page!";//returning string message...
//    }
//    @GetMapping("/home")
//    public String welcome(){
//
//        return "home";
//    }
//    @GetMapping("/hello/{name}")
//    public String sayHello(@PathVariable String name, Model model ){//
//        model.addAttribute("name",name);
//
//        return "hello1";
//    }
}
