package com.bravo.demo.controller;


import com.bravo.demo.model.Grocery;
import com.bravo.demo.repositiories.GroceryRepository;
import com.bravo.demo.services.GroceryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller

    public class GroceriesController {
//        @Autowired
//        private GroceryService groceryService;
//
//        @GetMapping("/groceries")
//        public String viewHomePage(Model model) {
//            return findPaginated(1, "name", "asc", model);
//        }
//
//        @GetMapping("/showNewGroceryForm")
//        public String showNewGroceryForm(Model model) {
//            Grocery grocery = new Grocery();
//            model.addAttribute("grocery", grocery);
//            return "/groceries/add-grocery";
//        }
//
//        @PostMapping("/saveGrocery")
//        public String saveGrocery(@ModelAttribute("grocery") Grocery grocery) {
//            groceryService.saveGrocery(grocery);
//            return "redirect:/groceries";
//        }
//
//        @GetMapping("/showFormUpdate/{id}")
//        public String showFormUpdate(@PathVariable(value = "id") long id, Model model) {
//            Grocery grocery = groceryService.getGroceryById(id);
//            model.addAttribute("grocery", grocery);
//            return "/groceries/update-grocery";
//        }
//
//        @GetMapping("/deleteGrocery/{id}")
//        public String deleteGrocery(@PathVariable(value = "id") long id) {
//            this.groceryService.deleteGroceryById(id);
//            return "redirect:/groceries";
//        }
//
//    @GetMapping("/pages/{pageNo}")
//    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
//                                @RequestParam("sortField") String sortField,
//                                @RequestParam("sortDir") String sortDir,
//                                Model model) {
//        int pageSize = 5;
//        Page<Grocery> page = groceryService.findPaginated(pageNo, pageSize, sortField, sortDir);
//        List<Grocery> listGrocery = page.getContent();
//        model.addAttribute("currentPage", pageNo);
//        model.addAttribute("totalPages", page.getTotalPages());
//        model.addAttribute("totalItems", page.getTotalElements());
//        model.addAttribute("sortField", sortField);
//        model.addAttribute("sortDir", sortDir);
//        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
//        model.addAttribute("listGrocery", listGrocery);
//        return "/groceries/index";
//    }

    //we must autowire to have access to the repo
    @Autowired
    GroceryRepository groceryRepository;


    @RequestMapping("/groceries")
    public String home(Model model){
        model.addAttribute("groceries", groceryRepository.findAll());//in here the findALL method come from the jpa
                return "groceries/index";//inside this model inside quotation "" variables to go to view
    }
    @RequestMapping(value = "/groceries/create", method = RequestMethod.GET)
    public String greetingForm(Model model){
        model.addAttribute("grocery", new Grocery());
        return "groceries/add-grocery";
    }

    @PostMapping("/groceries/create")
    public String greetingSubmit(@ModelAttribute("grocery") Grocery grocery) {
        groceryRepository.save(grocery);
        return "redirect:";

    }
}
