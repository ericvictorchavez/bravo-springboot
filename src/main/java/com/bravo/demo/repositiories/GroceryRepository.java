package com.bravo.demo.repositiories;


import com.bravo.demo.model.Grocery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroceryRepository extends JpaRepository<Grocery, Long> {

    Grocery findByItem (String Item);
}
